package br.com.syonet.exercicio03;

import java.util.List;
import java.util.stream.Collectors;

public class Veiculo {

	private String marca;
	private String modelo;
	private String placa;
	private String chassi;
	private Double valor;

	public Veiculo(String marca, String modelo, String placa, String chassi, Double valor) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.placa = placa;
		this.chassi = chassi;
		this.valor = valor;
	}

	public String toString() {
		return "Veiculo: " + marca + "\n" + "Modelo: " + modelo + "\n" + "Placa: " + placa + "\n" + "Chassi: " + chassi
				+ "\n" + "Valor: " + valor + "\n";

	}
	
	
	public static List<Veiculo> setarPlaca(List<Veiculo> veiculos) {
		List<Veiculo> listaPlacaAlteradas =  veiculos.stream()
				.filter(v -> v!=null)
				.peek(v -> v.placa = "ABC1234")
				.collect(Collectors.toList());
		return listaPlacaAlteradas;
		
				
}
	
} 
package br.com.syonet.exercicio01;

import java.util.List;

public class Veiculo {

	private String marca;
	private String modelo;
	private String placa;
	private String chassi;
	private Double valor;

	public Veiculo(String marca, String modelo, String placa, String chassi, Double valor) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.placa = placa;
		this.chassi = chassi;
		this.valor = valor;
	}

	public String toString() {
		return "Veiculo: " + marca + "\n" + "Modelo: " + modelo + "\n" + "Placa: " + placa + "\n" + "Chassi: " + chassi
				+ "\n" + "Valor: " + valor + "\n";

	}
	
	public static boolean somenteMarcaVW(List<Veiculo> veiculos) {
		return veiculos.stream()
				.filter(v -> v!=null)
				.allMatch(v -> v.marca.equals("VW"));		
	}
	
	public static boolean naoContemModeloGol(List<Veiculo> veiculos) {
		return veiculos.stream()
				.filter(v -> v!=null)
				.noneMatch(v -> v.modelo.equals("GOL"));		
	}
	
	public static boolean algumModeloJetta(List<Veiculo> veiculos) {
		return veiculos.stream()
				.filter(v -> v!=null)
				.anyMatch(v -> v.modelo.equals("JETTA"));		
	}
	
} 
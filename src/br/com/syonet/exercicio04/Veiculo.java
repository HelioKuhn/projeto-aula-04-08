package br.com.syonet.exercicio04;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Veiculo {

	private String marca;
	private String modelo;
	private String placa;
	private String chassi;
	private Double valor;

	public Veiculo(String marca, String modelo, String placa, String chassi, Double valor) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.placa = placa;
		this.chassi = chassi;
		this.valor = valor;
	}

	public String toString() {
		return "Veiculo: " + marca + "\n" + "Modelo: " + modelo + "\n" + "Placa: " + placa + "\n" + "Chassi: " + chassi
				+ "\n" + "Valor: " + valor + "\n";

	}

	public static List<Veiculo> listaDuplicada(List<Veiculo> veiculos) {
		List<Veiculo> listaDuplicadaVeiculos = veiculos.stream()
				.filter(v -> v!=null)
				.flatMap(v -> Stream.of(v, v))
				.collect(Collectors.toList());

		return listaDuplicadaVeiculos;
	}

}
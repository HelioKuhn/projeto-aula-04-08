package br.com.syonet.exercicio06;

import java.util.List;
import java.util.stream.Collectors;

public class Veiculo implements Comparable<Veiculo> {

	private String marca;
	private String modelo;
	private String placa;
	private String chassi;
	private Double valor;

	public Veiculo(String marca, String modelo, String placa, String chassi, Double valor) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.placa = placa;
		this.chassi = chassi;
		this.valor = valor;
	}

	public String toString() {
		return "Veiculo: " + marca + "\n" + "Modelo: " + modelo + "\n" + "Placa: " + placa + "\n" + "Chassi: " + chassi
				+ "\n" + "Valor: " + valor + "\n";

	}

	public static List<Veiculo> listarPorPrecos(List<Veiculo> veiculos) {
		return veiculos.stream()
				.filter(v -> v!=null)
				.sorted((v1, v2) -> v1.compareTo(v2)).collect(Collectors.toList());

	}

	@Override
	public int compareTo(Veiculo outroVeiculo) {
		if (this.valor > outroVeiculo.valor) {
			return -1;
		}
		if (this.valor < outroVeiculo.valor) {
			return 1;
		}
		return 0;
	}

}
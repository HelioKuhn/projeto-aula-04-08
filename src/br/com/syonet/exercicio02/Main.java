package br.com.syonet.exercicio02;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<Veiculo> veiculos = Arrays.asList(
				new Veiculo("VW", "CIVIC", "JDJ-8939", "DK3SJ3939S9D9JSK", 22500.00),
				new Veiculo("VW", "JETTA", "UFU-3892", "AHH3JKA8U309AS9I3", 45000.00),
				new Veiculo("VW", "I10", "AHD-4828", "ASHAQOIASIUDIUQ2", 15500.00),
				new Veiculo("VW", "GOLF", "JDJ-8939", "DK3SJ3939S9D9JSK", 78000.00),
				new Veiculo("VW", "GOL","TOI-5939","SH3HAJFU99WE9JDJ", 56938.00));
		
		
		
		System.out.println("Total da soma dos preços dos veículos é :  " + Veiculo.totalPrecoVeiculos(veiculos));

}

}
